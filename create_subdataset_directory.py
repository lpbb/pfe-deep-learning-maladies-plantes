import os
from os.path import join, isfile, exists
from dotenv import load_dotenv, find_dotenv
import shutil
from PIL import Image
# Créer un fichier .env à la racine avec

load_dotenv(find_dotenv())
#PATH_TO_ROI_TYP_FOR_KERAS = os.environ.get("PATH_TO_ROI_TYP_FOR_KERAS")
PATH_TO_ROI_TYP_FOR_MULTIVIEW = os.environ.get("PATH_TO_ROI_TYP_FOR_MULTIVIEW")
PATH_TO_SMALL_ROI_TYP_FOR_KERAS = os.environ.get(
    "PATH_TO_SMALL_ROI_TYP_FOR_KERAS")

percent = 0.2
views = ['RECTO', 'VERSO']
classes = {
    'Alt': 1967,
    'Big': 1326,
    'Mac': 1546,
    'Mil': 1728,
    'Myc': 1544,
    'Pse': 1578,
    'Syl': 1780
}

# On parcourt les vues
for v in views:
    if not exists(join(PATH_TO_SMALL_ROI_TYP_FOR_KERAS, v)):
        # on la créé
        os.makedirs(join(PATH_TO_SMALL_ROI_TYP_FOR_KERAS, v))
    # On parcourt les classes
    for c in classes.keys():
        # Si le dossier avec la classe n'existe pas dans le dossier PATH_TO_SMALL_ROI_TYP_FOR_KERAS
        if not exists(join(PATH_TO_SMALL_ROI_TYP_FOR_KERAS, v, c)):
            # on la créé
            os.makedirs(join(PATH_TO_SMALL_ROI_TYP_FOR_KERAS, v, c))
# On parcourt les vues
for v in views : 
    # On parcourt les fichiers de PATH_TO_ROI_TYP_FOR_MULTIVIEW
    for dossier, num_file in classes.items():
        class_path = join(PATH_TO_ROI_TYP_FOR_MULTIVIEW, v, dossier)
        num_file = round(num_file * percent)
        compteur = 0
        for file in sorted(os.listdir(class_path)):
            if compteur <= num_file:
                if isfile(join(class_path, file)):
                    try:
                        shutil.copy(join(class_path, file), join(PATH_TO_SMALL_ROI_TYP_FOR_KERAS,v,dossier,file))
                    
                    # If source and destination are same
                    except shutil.SameFileError:
                        print("Source and destination represents the same file.")
            elif compteur > num_file:
                break
            compteur += 1

    # TO DO : On vérifie que le dernier fichier n'est pas un recto
