import os
from os.path import join, isfile, exists
from dotenv import load_dotenv, find_dotenv
import shutil
from PIL import Image
#Créer un fichier .env à la racine avec 
#PATH_TO_ROI_TYP = full_path/to/Images_ROI_Typ exemple : /home/ensai/Bureau/3A/PFE/data/Images_ROI/Images_ROI_Typ
#PATH_TO_ROI_TYP_FOR_KERAS full_path/to/folder où copier les données pour keras exemple : /home/ensai/Bureau/3A/PFE/data/Images_ROI/Images_ROI_Typ_for_keras
load_dotenv(find_dotenv())
PATH_TO_ROI_TYP = os.environ.get("PATH_TO_ROI_TYP")
PATH_TO_ROI_TYP_FOR_MULTIVIEW = os.environ.get("PATH_TO_ROI_TYP_FOR_MULTIVIEW")

VIEWS = ["RECTO","VERSO"]
CLASSES = ["Alt","Big","Mac","Mil","Myc","Pse","Syl"]

# On parcourt les vues
for v in VIEWS:
    if not exists(join(PATH_TO_ROI_TYP_FOR_MULTIVIEW,v)):
            #on la créé
            os.makedirs(join(PATH_TO_ROI_TYP_FOR_MULTIVIEW,v))
    #On parcourt les classes
    for c in CLASSES:
        #Si le dossier avec la classe n'existe pas dans le dossier PATH_TO_ROI_TYP_FOR_KERAS
        if not exists(join(PATH_TO_ROI_TYP_FOR_MULTIVIEW,v,c)):
            #on la créé
            os.makedirs(join(PATH_TO_ROI_TYP_FOR_MULTIVIEW,v,c))

#On parcourt les fichiers de PATH_TO_ROI_TYP (toutes les images)
for f in os.listdir(PATH_TO_ROI_TYP):
    if isfile(join(PATH_TO_ROI_TYP,f)):
        image = Image.open(join(PATH_TO_ROI_TYP,f))
        file_name = f[:-5] + ".jpeg"
        #On parcourt les classes
        for c in CLASSES:
            #Si la classe se trouve dans le nom de fichier
            if c.lower() in f.lower():
                        for v in VIEWS: 
                            if v.lower() in f.lower():
                                #Si l'image n'existe pas dans le répertoire correspondant à sa classe
                                if not exists(join(PATH_TO_ROI_TYP_FOR_MULTIVIEW,v,c,file_name)):
                                    #On copie l'image dans le dossier correspondant à la classe
                                    #shutil.copyfile(join(PATH_TO_ROI_TYP,f),join(PATH_TO_ROI_TYP_FOR_KERAS,c,f))
                                    image.save(join(PATH_TO_ROI_TYP_FOR_MULTIVIEW,v,c,file_name))
                                    image.close()